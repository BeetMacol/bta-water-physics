package com.beetmacol.mc.bta_water_physics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import net.minecraft.core.block.Block;
import net.minecraft.core.block.entity.TileEntity;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.enums.EnumDropCause;
import net.minecraft.core.item.ItemStack;
import net.minecraft.core.util.helper.Direction;
import net.minecraft.core.util.helper.Side;
import net.minecraft.core.util.phys.AABB;
import net.minecraft.core.world.World;
import net.minecraft.core.world.WorldSource;

public class FluidBlock extends Block {
	public static final int LEVEL_COUNT = 8;
	public final int tickRateVal;

	public FluidBlock(String key, int id, Material material, int tickRate) {
		// TODO replace tick rate with density and also use if for other stuff than ticking?
		super(key, id, material);
		this.tickRateVal = tickRate;
	}

	@Override
	public int tickRate() {
		return 2; // FIXME temp
//		return this.tickRateVal;
	}

	@Override
	public AABB getCollisionBoundingBoxFromPool(WorldSource world, int x, int y, int z) {
		return null;
	}

	@Override
	public void setBlockBoundsBasedOnState(WorldSource world, int x, int y, int z) {
		double level = getMaxHeight(world, x, y, z);
		this.setBlockBounds(0.0, 0.0, 0.0, 1.0, level, 1.0);
	}

	@Override
	public ItemStack[] getBreakResult(World world, EnumDropCause dropCause, int x, int y, int z, int meta, TileEntity tileEntity) {
		return null;
	}

	@Override
	public boolean isSolidRender() {
		return false;
	}

	@Override
	public boolean canCollideCheck(int meta, boolean shouldCollideWithFluids) {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean getIsBlockSolid(WorldSource blockAccess, int x, int y, int z, Side side) {
		return false;
	}

	static int getLevel(WorldSource world, int x, int y, int z) {
		return world.getBlockMetadata(x, y, z) % LEVEL_COUNT;
	}
	static void setLevel(World world, int x, int y, int z, int val) {
		world.setBlockMetadata(x, y, z, val);
	}

	void updateLevel(World world, int x, int y, int z, int level) {
		if (world.getBlockId(x, y, z) == this.id) {
			world.setBlockMetadata(x, y, z, level);
			world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
		} else {
			world.setBlockAndMetadata(x, y, z, id, level);
		}
		world.markBlockNeedsUpdate(x, y, z);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int blockId) {
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}

	static double getMaxHeight(int level) {
		return (double) (level + 1) / (double) (LEVEL_COUNT);
	}
	static double getMaxHeight(WorldSource world, int x, int y, int z) {
		return getMaxHeight(getLevel(world, x, y, z));
	}

	List<Direction> spreadDirections(World world, int x, int y, int z, Random rand, boolean onlyPriority) {
		ArrayList<Direction> directions = new ArrayList<>(4);
		List<Direction> horizontalDirections = Arrays.asList(Direction.horizontalDirections);
		Collections.shuffle(horizontalDirections, rand);
		for (Direction dir : horizontalDirections) {
			int id = world.getBlockId(x + dir.getOffsetX(), y - 1, z + dir.getOffsetZ());
			if (id == 0 || id == this.id) {
				directions.add(dir);
			}
		}
		if (onlyPriority) return directions;
		for (Direction dir : horizontalDirections) {
			if (directions.contains(dir)) continue;
			directions.add(dir);
		}
		return directions;
	}

	void spread(World world, int x, int y, int z) {
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		int level = getLevel(world, x, y, z);
		int id = world.getBlockId(x, y - 1, z);
		if (id == 0) {
			this.updateLevel(world, x, y - 1, z, level);
			world.setBlock(x, y, z, 0);
			world.markBlockNeedsUpdate(x, y, z);
			return;
		} else if (id == this.id) {
			int levelBelow = getLevel(world, x, y - 1, z);
			int left = LEVEL_COUNT - levelBelow;
			// FIXME bugged
			if (left > 0) {
				level -= left - 1;
				if (level < 0) {
					world.setBlock(x, y, z, 0);
					left += level;
				}
				world.markBlockNeedsUpdate(x, y, z);
				this.updateLevel(world, x, y - 1, z, levelBelow + left);
				if (level < 0) return;
			}
		}
//		if (level == 0) return;
		List<Direction> directions = this.spreadDirections(world, x, y, z, rand, level == 0);
		int[] levels = { LEVEL_COUNT, LEVEL_COUNT, LEVEL_COUNT, LEVEL_COUNT };
		int i = 0;
		for (Direction dir : directions) {
			id = world.getBlockId(x + dir.getOffsetX(), y, z + dir.getOffsetZ());
			if (id == this.id) {
				levels[i] = getLevel(world, x + dir.getOffsetX(), y, z + dir.getOffsetZ());
			} else if (id == 0) {
				levels[i] = -1;
			}
			i++;
		}
		int[] levelsOrig = levels.clone();
		while (level > -1) { // FIXME after getting to level 0, only priority ones should be filled...
			int smallest = levels.length;
			int smallestLevel = LEVEL_COUNT;
			for (i = 0; i < levels.length; i++) {
				if (levels[i] >= smallestLevel) continue;
				smallest = i;
				smallestLevel = levels[i];
			}
			if (smallest == levels.length || smallestLevel >= level) break;
			levels[smallest]++;
			level--;
		}
		i = 0;
		for (Direction dir : directions) {
			int xPos = x + dir.getOffsetX();
			int zPos = z + dir.getOffsetZ();
			if (levels[i] >= 0 && levels[i] != levelsOrig[i]) {
				this.updateLevel(world, xPos, y, zPos, levels[i]);
			}
			i++;
		}
		if (level < 0) {
			world.setBlock(x, y, z, 0);
			world.markBlockNeedsUpdate(x, y, z);
		} else if (level != getLevel(world, x, y, z)) {
			this.updateLevel(world, x, y, z, level);
		} else return;
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		super.onBlockAdded(world, x, y, z);
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}
}
