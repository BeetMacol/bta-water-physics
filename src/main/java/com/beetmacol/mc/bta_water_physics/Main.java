package com.beetmacol.mc.bta_water_physics;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import turniplabs.halplibe.helper.BlockBuilder;
import turniplabs.halplibe.helper.ItemBuilder;
import turniplabs.halplibe.helper.recipeBuilders.RecipeBuilderShaped;
import turniplabs.halplibe.util.GameStartEntrypoint;
import turniplabs.halplibe.util.RecipeEntrypoint;
import net.minecraft.client.render.block.model.BlockModelDispatcher;
import net.minecraft.client.render.item.model.ItemModelStandard;
import net.minecraft.core.block.Block;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.block.tag.BlockTags;
import net.minecraft.core.item.Item;

public class Main implements ModInitializer, GameStartEntrypoint, RecipeEntrypoint {
    public static final String MOD_ID = "bta-water-physics";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	public static FluidBlock WATER;
	public static FluidBucket WATER_BUCKET;
	public static MopItem MOP;
	public static InfiniteWellBlock INFINITE_WELL;

	static int next_id_ = 7300;
	public int next_id() {
		return next_id_++;
	}

    @Override
    public void onInitialize() {
		WATER = new BlockBuilder(MOD_ID)
			.setHardness(100.0f)
			.setTopTexture("minecraft:block/water_top")
			.setBottomTexture("minecraft:block/water_bottom")
			.setSideTextures("minecraft:block/water_side")
			.setLightOpacity(3)
			.setTags(BlockTags.IS_WATER, BlockTags.PLACE_OVERWRITES, BlockTags.NOT_IN_CREATIVE_MENU)
			.build(new FluidBlock(MOD_ID + ".water", next_id(), Material.water, 5));
//		Block.neighborNotifyOnMetadataChangeDisabled[WATER.id] = true;
		WATER_BUCKET = new ItemBuilder(MOD_ID).setItemModel(item -> new ItemModelStandard(item, MOD_ID)).setKey(MOD_ID + ".water-bucket").build(new FluidBucket(next_id(), WATER.id));
		MOP = new ItemBuilder(MOD_ID).setIcon(MOD_ID + ":item/mop").build(new MopItem("mop", next_id()));
		INFINITE_WELL = new BlockBuilder(MOD_ID).build(new InfiniteWellBlock(MOD_ID + ".infinite_well", next_id(), Material.water));
	}

	@Override
	public void beforeGameStart() {
	}

	@Override
	public void afterGameStart() {
		FluidModel model = new FluidModel(WATER);
		BlockModelDispatcher.getInstance().addDispatch(WATER, model);
//		model.atlasIndices[Side.TOP.getId()] = TextureRegistry.getTexture("minecraft:block/water_top");
	}

	@Override
	public void onRecipesReady() {
		new RecipeBuilderShaped(MOD_ID, "WSW", " I ", " I ")
			.addInput('W', Block.wool)
			.addInput('S', Block.spongeDry)
			.addInput('I', Item.stick)
			.create("mop", MOP.getDefaultStack());
	}

	@Override
	public void initNamespaces() {
	}
}
