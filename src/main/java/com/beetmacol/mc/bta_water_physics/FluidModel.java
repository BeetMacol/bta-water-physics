package com.beetmacol.mc.bta_water_physics;

import net.minecraft.client.render.block.model.BlockModelStandard;
import net.minecraft.client.render.tessellator.Tessellator;
import net.minecraft.core.block.Block;

public class FluidModel extends BlockModelStandard<FluidBlock> {
	public FluidModel(Block block) {
		super(block);
	}

	@Override
	public boolean render(Tessellator tessellator, int x, int y, int z) {
		// TODO if renderBlocks.renderAllFaces is false, only render visible
//		float level = FluidBlock.getMaxHeight(renderBlocks.blockAccess, x, y, z);

		block.setBlockBoundsBasedOnState(renderBlocks.blockAccess, x, y, z);
		return this.renderStandardBlock(tessellator, this.block, x, y, z);
	}

	@Override
	public void renderBlockOnInventory(Tessellator tessellator, int x, float y, float z) {
	}
}
