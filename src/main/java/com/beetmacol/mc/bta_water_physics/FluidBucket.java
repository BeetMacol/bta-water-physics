package com.beetmacol.mc.bta_water_physics;

import net.minecraft.core.block.Block;
import net.minecraft.core.entity.player.EntityPlayer;
import net.minecraft.core.enums.EnumDropCause;
import net.minecraft.core.item.Item;
import net.minecraft.core.item.ItemStack;
import net.minecraft.core.util.helper.Side;
import net.minecraft.core.world.World;

public class FluidBucket extends Item {
	int blockId;
	public FluidBucket(int id, int blockId) {
		super(id);
		this.blockId = blockId;
	}

	@Override
	public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int blockX, int blockY, int blockZ, Side side, double xPlaced, double yPlaced) {
		int x = blockX + side.getOffsetX();
		int y = blockY + side.getOffsetY();
		int z = blockZ + side.getOffsetZ();
		if (world.getBlockId(x, y, z) != 0) {
			Block block = world.getBlock(x, y, z);
			if (block.getIsBlockSolid(world, x, y, z, side)) return false;
			block.dropBlockWithCause(world, EnumDropCause.WORLD, x, y, z, world.getBlockMetadata(x, y, z), null);
		}
		world.setBlockAndMetadataWithNotify(x, y, z, this.blockId, FluidBlock.LEVEL_COUNT - 1);
		return true;
	}
}
