package com.beetmacol.mc.bta_water_physics;

import java.util.Random;
import net.minecraft.core.block.Block;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.world.World;

public class InfiniteWellBlock extends Block {
	public InfiniteWellBlock(String key, int id, Material material) {
		super(key, id, material);
		this.setTicking(true);
	}

	@Override
	public int tickRate() {
		return 10;
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		super.onBlockAdded(world, x, y, z);
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		boolean isPoweredByBlock= world.isBlockGettingPowered(x, y, z) || world.isBlockIndirectlyGettingPowered(x, y, z);
		if (isPoweredByBlock) {
			y += 1;
			world.setBlockAndMetadataWithNotify(x, y, z, Main.WATER.id, FluidBlock.LEVEL_COUNT - 1);
			world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
		}
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int blockId) {
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}
}
