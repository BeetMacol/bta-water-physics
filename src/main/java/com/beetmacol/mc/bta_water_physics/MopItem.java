package com.beetmacol.mc.bta_water_physics;

import net.minecraft.core.entity.player.EntityPlayer;
import net.minecraft.core.item.ItemStack;
import net.minecraft.core.item.material.ToolMaterial;
import net.minecraft.core.item.tool.ItemTool;
import net.minecraft.core.util.helper.Direction;
import net.minecraft.core.util.helper.Side;
import net.minecraft.core.world.World;

public class MopItem extends ItemTool {
	public MopItem(String name, int id) {
		super(name, id, 0, ToolMaterial.wood, null);
		this.setMaxDamage(1536);
	}

	int drainFluid(World world, int x, int y, int z, int maxLayers) {
		if (world.getBlockId(x, y, z) != Main.WATER.id) return 0;
		int level = FluidBlock.getLevel(world, x, y, z);
		if (level <= maxLayers) {
			world.setBlockWithNotify(x, y, z, 0);
			return level + 1;
		}
		FluidBlock.setLevel(world, x, y, z, level - maxLayers);
		world.notifyBlockChange(x, y, z, Main.WATER.id);
		return maxLayers;
	}

	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer entity, World world, int blockX, int blockY, int blockZ, Side side, double xPlaced, double yPlaced) {
		int x = blockX + side.getOffsetX();
		int y = blockY + side.getOffsetY();
		int z = blockZ + side.getOffsetZ();
		int drained = this.drainFluid(world, x, y, z, 3);
		for (Direction dir : Direction.horizontalDirections) {
			drained += this.drainFluid(world, x + dir.getOffsetX(), y + dir.getOffsetY(), z + dir.getOffsetZ(), 2);
		}
		stack.damageItem(drained, entity);
		return drained > 0;
	}
}
